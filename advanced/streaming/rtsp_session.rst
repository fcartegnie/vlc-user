.. _stream-with-rtsp:

###########################
Stream using RTSP
###########################

RTSP is a session management protocol. RTSP negotiates the streaming parameters (like the IP address of the client, the port at which the streaming will happen, what is the muxer involved, etc). for establishing the connection between the sender and the receiver. 

After the connection is established, RTP is used for transporting the data stream. To understand how RTP streaming works, refer to :ref:`Stream over RTP <stream-over-rtp>`.

Streaming using RTSP is similar to how a pull protocol works because the client "requests" for the stream. Once the request reaches the server, they negotiate the parameters (using RTSP), and then the sender sends the data to the receiving device (using RTP). 

Streaming over RTSP is fundamentally different from multicast streaming over RTP in the sense that in case of multicast-streaming over RTP, the data is "pushed" to all the devices in the local network (as RTP works on the push protocol). However, in the case of streaming using RTSP, a new RTP stream is established for every client, and hence data is only sent to a client after it has requested for the stream.

This page describes how to stream using RTSP over the local network. As a reminder, note that streaming over the local network means that the streaming device and all the receiving device(s) should be connected to the same router for the streaming to work. 


.. _send-rtsp-stream:

**************************
Sending the stream
**************************

For this example, let's assume that the IP address of the streaming device is ``192.168.0.101``. To stream a file (``sample.mp4``) over port number ``8090`` and path name ``sample`` (you can choose your own port and path-name), the code will be:

.. code-block ::

   $ vlc sample.mp4 --sout="#rtp{sdp=rtsp://:8090/stream}"

Or, you can specify the IP address of the streaming device like in the following code:

.. code-block ::
   
   $ vlc sample.mp4 --sout="#rtp{sdp=rtsp://192.168.0.101:8090/stream}"



.. _receive-rtsp-stream:

************************
Receiving the stream
************************

To start receiving, we need to simply connect to the RTSP stream. This can be done by running the following code on the terminal of the client device(s):

.. code-block ::

   $ vlc rtsp://192.168.0.101:8090/stream

Note that we need to specify the IP address of the streaming device for receiving the stream.

Diagramatically, this is how the network might look:

.. graphviz::

	digraph rtsp {
    rankdir=TB;
    node [shape=circle];
    
    { node [width=0 shape=point label=""]; idle;
    }
    
    { node [shape=plaintext]; "$ vlc sample.mp4 --sout=\n\"#rtp{sdp=rtsp://:8090/stream}\"",  "$ vlc rtsp://192.168.0.101:8090/stream";
    }
    
    
    "Receiving Device \n IP: 192.168.0.102" -> "Streaming Device \n IP: 192.168.0.101" [label="This device connects to the RTSP\nstream, which negotiates details for\nstarting a new RTP stream."]
    "Streaming Device \n IP: 192.168.0.101" -> "Receiving Device \n IP: 192.168.0.102" [label="The server then starts a new\nclient-specific RTP stream and\nsends the data to the receiver"] 


    "Streaming Device \n IP: 192.168.0.101":w -> "$ vlc sample.mp4 --sout=\n\"#rtp{sdp=rtsp://:8090/stream}\"":w [style=invis]
    "Receiving Device \n IP: 192.168.0.102" -> "$ vlc rtsp://192.168.0.101:8090/stream" [style=invis]
    "Idle Device \n IP: 192.168.0.103" -> "Receiving Device \n IP: 192.168.0.102" [style=invis]
    
    idle -> "Streaming Device \n IP: 192.168.0.101" [arrowhead=none]
    idle -> "Idle Device \n IP: 192.168.0.103" [arrowhead=none] [label="This device is idle because it has not requested for\nthe stream. Although, it can send a request any\ntime and begin receiving."]

   subgraph devices {
    rank=same;
    "Streaming Device \n IP: 192.168.0.101", "Receiving Device \n IP: 192.168.0.102";
    }
    
    subgraph arrow {
    rank=same;
    idle, "Idle Device \n IP: 192.168.0.103";
    }

  }

|